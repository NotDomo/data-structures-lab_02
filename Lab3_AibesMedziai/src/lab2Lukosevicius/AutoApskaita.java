package lab2Lukosevicius;

import laborai.studijosktu.BstSetKTU;
import laborai.studijosktu.SetADT;

public class AutoApskaita {

    public static SetADT<String> autobusuMarkes(Autobusas[] auto) {
        SetADT<Autobusas> uni = new BstSetKTU<>(Autobusas.pagalMarke);
        SetADT<String> kart = new BstSetKTU<>();
        for (Autobusas a : auto) {
            int sizeBefore = uni.size();
            uni.add(a);

            if (sizeBefore == uni.size()) {
                kart.add(a.getMarkė());
            }
        }
        return kart;
    }
}
