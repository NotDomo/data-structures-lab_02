package lab2Lukosevicius;

import laborai.gui.MyException;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

public class AutoGamyba {

    private static Autobusas[] autobusai;
    private static int pradinisIndeksas = 0, galinisIndeksas = 0;
    private static boolean arPradzia = true;

    public static Autobusas[] generuoti(int kiekis) {
        autobusai = new Autobusas[kiekis];
        for (int i = 0; i < kiekis; i++) {
            autobusai[i] = new Autobusas.Builder().buildRandom();
        }
        return autobusai;
    }

    public static Autobusas[] generuotiIrIsmaisyti(int aibesDydis,
            double isbarstymoKoeficientas) throws MyException {
        return generuotiIrIsmaisyti(aibesDydis, aibesDydis, isbarstymoKoeficientas);
    }

    /**
     *
     * @param aibesDydis
     * @param aibesImtis
     * @param isbarstymoKoeficientas
     * @return Gražinamas aibesImtis ilgio masyvas
     * @throws MyException
     */
    public static Autobusas[] generuotiIrIsmaisyti(int aibesDydis,
            int aibesImtis, double isbarstymoKoeficientas) throws MyException {
        autobusai = generuoti(aibesDydis);
        return ismaisyti(autobusai, aibesImtis, isbarstymoKoeficientas);
    }

    // Galima paduoti masyvą išmaišymui iš išorės
    public static Autobusas[] ismaisyti(Autobusas[] autoBaze,
            int kiekis, double isbarstKoef) throws MyException {
        if (autoBaze == null) {
            throw new IllegalArgumentException("AutoBaze yra null");
        }
        if (kiekis <= 0) {
            throw new MyException(String.valueOf(kiekis), 1);
        }
        if (autoBaze.length < kiekis) {
            throw new MyException(autoBaze.length + " >= " + kiekis, 2);
        }
        if ((isbarstKoef < 0) || (isbarstKoef > 1)) {
            throw new MyException(String.valueOf(isbarstKoef), 3);
        }

        int likusiuKiekis = autoBaze.length - kiekis;
        int pradziosIndeksas = (int) (likusiuKiekis * isbarstKoef / 2);

        Autobusas[] pradineAutobusuImtis = Arrays.copyOfRange(autoBaze, pradziosIndeksas, pradziosIndeksas + kiekis);
        Autobusas[] likusiAutobusuImtis = Stream
                .concat(Arrays.stream(Arrays.copyOfRange(autoBaze, 0, pradziosIndeksas)),
                        Arrays.stream(Arrays.copyOfRange(autoBaze, pradziosIndeksas + kiekis, autoBaze.length)))
                .toArray(Autobusas[]::new);

        Collections.shuffle(Arrays.asList(pradineAutobusuImtis)
                .subList(0, (int) (pradineAutobusuImtis.length * isbarstKoef)));
        Collections.shuffle(Arrays.asList(likusiAutobusuImtis)
                .subList(0, (int) (likusiAutobusuImtis.length * isbarstKoef)));

        AutoGamyba.pradinisIndeksas = 0;
        galinisIndeksas = likusiAutobusuImtis.length - 1;
        AutoGamyba.autobusai = likusiAutobusuImtis;
        return pradineAutobusuImtis;
    }

    public static Autobusas gautiIsBazes() throws MyException {
        if ((galinisIndeksas - pradinisIndeksas) < 0) {
            throw new MyException(String.valueOf(galinisIndeksas - pradinisIndeksas), 4);
        }
        // Vieną kartą Autobusas imamas iš masyvo pradžios, kitą kartą - iš galo.
        arPradzia = !arPradzia;
        return autobusai[arPradzia ? pradinisIndeksas++ : galinisIndeksas--];
    }
}
