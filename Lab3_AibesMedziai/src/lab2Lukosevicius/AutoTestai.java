package lab2Lukosevicius;

import laborai.studijosktu.Ks;
import laborai.studijosktu.AvlSetKTUx;
import laborai.studijosktu.SortedSetADTx;
import laborai.studijosktu.SetADT;
import laborai.studijosktu.BstSetKTUx;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;

/*
 * Aibės testavimas be Swing'o
 *
 */
public class AutoTestai {

    static Autobusas[] autoBaze;
    static SortedSetADTx<Autobusas> aSerija = new BstSetKTUx(new Autobusas(), Autobusas.pagalKaina);

    public static void main(String[] args) throws CloneNotSupportedException {
        Locale.setDefault(Locale.US); // Suvienodiname skaičių formatus
        aibėsTestas();
    }

    static SortedSetADTx generuotiAibe(int kiekis, int generN) {
        autoBaze = new Autobusas[generN];
        for (int i = 0; i < generN; i++) {
            autoBaze[i] = new Autobusas.Builder().buildRandom();
        }
        Collections.shuffle(Arrays.asList(autoBaze));
        aSerija.clear();
        for (int i = 0; i < kiekis; i++) {
            aSerija.add(autoBaze[i]);
        }
        return aSerija;
    }

    public static void aibėsTestas() throws CloneNotSupportedException {
        Autobusas a1 = new Autobusas("Ford", "Transit", 2006, 50000, 2500);
        Autobusas a2 = new Autobusas.Builder()
                .markė("Ford")
                .modelis("Transit")
                .gamMetai(2001)
                .rida(20000)
                .kaina(3500)
                .build();
        Autobusas a3 = new Autobusas.Builder().buildRandom();
        Autobusas a4 = new Autobusas("Ford Transit 2005 115900 2800");
        Autobusas a5 = new Autobusas("Ford Transit 2007 365100 3200");
        Autobusas a6 = new Autobusas("Mercedes Benz 2001 666666 666.6");
        Autobusas a7 = new Autobusas("Ford Transit 2006 55555 4000");
        Autobusas a8 = new Autobusas("Ford Transit 2008 44444 4200");
        Autobusas a9 = new Autobusas("Mercedes Benz 2000 420000 850.3");

        Autobusas[] autoMasyvas = {a9, a7, a8, a5, a1, a6};

        Ks.oun("Auto Aibė:");
        SortedSetADTx<Autobusas> autoAibe = new BstSetKTUx(new Autobusas());

        for (Autobusas a : autoMasyvas) {
            autoAibe.add(a);
            Ks.oun("Aibė papildoma: " + a + ". Jos dydis: " + autoAibe.size());
        }
        Ks.oun("");
        Ks.oun(autoAibe.toVisualizedString(""));

        SortedSetADTx<Autobusas> autoAibeKopija
                = (SortedSetADTx<Autobusas>) autoAibe.clone();

        autoAibeKopija.add(a2);
        autoAibeKopija.add(a3);
        autoAibeKopija.add(a4);
        Ks.oun("Papildyta autoaibės kopija:");
        Ks.oun(autoAibeKopija.toVisualizedString(""));

        a9.setRida(10000);

        Ks.oun("Originalas:");
        Ks.ounn(autoAibe.toVisualizedString(""));

        Ks.oun("Ar elementai egzistuoja aibėje?");
        for (Autobusas a : autoMasyvas) {
            Ks.oun(a + ": " + autoAibe.contains(a));
        }
        Ks.oun(a2 + ": " + autoAibe.contains(a2));
        Ks.oun(a3 + ": " + autoAibe.contains(a3));
        Ks.oun(a4 + ": " + autoAibe.contains(a4));
        Ks.oun("");

        Ks.oun("Ar elementai egzistuoja aibės kopijoje?");
        for (Autobusas a : autoMasyvas) {
            Ks.oun(a + ": " + autoAibeKopija.contains(a));
        }
        Ks.oun(a2 + ": " + autoAibeKopija.contains(a2));
        Ks.oun(a3 + ": " + autoAibeKopija.contains(a3));
        Ks.oun(a4 + ": " + autoAibeKopija.contains(a4));
        Ks.oun("");

        Ks.oun("Elementų šalinimas iš kopijos. Aibės dydis prieš šalinimą:  " + autoAibeKopija.size());
        for (Autobusas a : new Autobusas[]{a2, a1, a9, a8, a5, a3, a4, a2, a7, a6, a7, a9}) {
            autoAibeKopija.remove(a);
            Ks.oun("Iš autoaibės kopijos pašalinama: " + a + ". Jos dydis: " + autoAibeKopija.size());
        }
        Ks.oun("");

        Ks.oun("Automobilių aibė su iteratoriumi:");
        Ks.oun("");
        for (Autobusas a : autoAibe) {
            Ks.oun(a);
        }
        Ks.oun("");
        Ks.oun("Automobilių aibė AVL-medyje:");
        SortedSetADTx<Autobusas> autoAibe3 = new AvlSetKTUx(new Autobusas());
        for (Autobusas a : autoMasyvas) {
            autoAibe3.add(a);
        }
        Ks.ounn(autoAibe3.toVisualizedString(""));

        Ks.oun("Automobilių aibė su iteratoriumi:");
        Ks.oun("");
        for (Autobusas a : autoAibe3) {
            Ks.oun(a);
        }

        Ks.oun("");
        Ks.oun("Automobilių aibė su atvirkštiniu iteratoriumi:");
        Ks.oun("");
        Iterator iter = autoAibe3.descendingIterator();
        while (iter.hasNext()) {
            Ks.oun(iter.next());
        }

        Ks.oun("");
        Ks.oun("Automobilių aibės toString() metodas:");
        Ks.ounn(autoAibe3);

        // Išvalome ir suformuojame aibes skaitydami iš failo
        autoAibe.clear();
        autoAibe3.clear();

        Ks.oun("");
        Ks.oun("Automobilių aibė DP-medyje:");
        autoAibe.load("Duomenys\\ban.txt");
        Ks.ounn(autoAibe.toVisualizedString(""));
        Ks.oun("Išsiaiškinkite, kodėl medis augo tik į vieną pusę.");

        Ks.oun("");
        Ks.oun("Automobilių aibė AVL-medyje:");
        autoAibe3.load("Duomenys\\ban.txt");
        Ks.ounn(autoAibe3.toVisualizedString(""));

        SetADT<String> autoAibe4 = AutoApskaita.autobusuMarkes(autoMasyvas);
        Ks.oun("Pasikartojančios automobilių markės:\n" + autoAibe4.toString());
    }
}
