package lab2Lukosevicius;

import laborai.studijosktu.BstSetKTUx2;
import laborai.studijosktu.AvlSetKTUx;
import laborai.studijosktu.SortedSetADTx;
import laborai.studijosktu.BstSetKTUx;
import laborai.gui.MyException;
import java.util.ResourceBundle;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;

public class GreitaveikosTyrimas {

    public static final String FINISH_COMMAND = "finish";
    private static final ResourceBundle MESSAGES = ResourceBundle.getBundle("laborai.gui.messages");

    private static final String[] TYRIMU_VARDAI = {"addBstRec", "addBstIte", "addAvlRec", "removeBst"};
    private static final int[] TIRIAMI_KIEKIAI = {20000, 40000, 80000, 160000};

    private final BlockingQueue resultsLogger = new SynchronousQueue();
    private final Semaphore semaphore = new Semaphore(-1);
    private final Timekeeper tk;
    private final String[] errors;

    private final SortedSetADTx<Autobusas> aSeries = new BstSetKTUx(new Autobusas(), Autobusas.pagalKaina);
    private final SortedSetADTx<Autobusas> aSeries2 = new BstSetKTUx2(new Autobusas());
    private final SortedSetADTx<Autobusas> aSeries3 = new AvlSetKTUx(new Autobusas());

    public GreitaveikosTyrimas() {
        semaphore.release();
        tk = new Timekeeper(TIRIAMI_KIEKIAI, resultsLogger, semaphore);
        errors = new String[]{
            MESSAGES.getString("error1"),
            MESSAGES.getString("error2"),
            MESSAGES.getString("error3"),
            MESSAGES.getString("error4")
        };
    }

    private void individualusMetodai() throws InterruptedException{
        TreeSet<Integer> treeSet = new TreeSet<>();
        HashSet<Integer> hashSet = new HashSet<>();

        try
        {
            for (int kiekis : TIRIAMI_KIEKIAI) {
                int[] numbers = generateElements(100000);
                treeSet.clear();
                hashSet.clear();
                tk.startAfterPause();
                tk.start();
                for (int a : numbers) {
                  treeSet.add(a);
                }
                tk.finish("addTreeSet");
                for (int a : numbers) {
                  hashSet.add(a);
                }
                tk.finish("addHashSet");
                for (int a : numbers) {
                  treeSet.remove(a);
                }
                tk.finish("removeTreeSet");

             for (int a : numbers) {
                  hashSet.remove(a);
                }
                tk.finish("removeHashSet");

                tk.seriesFinish();
            }
            tk.logResult(FINISH_COMMAND);
        }
        catch (MyException e) {
            if (e.getCode() >= 0 && e.getCode() <= 3) {
                tk.logResult(errors[e.getCode()] + ": " + e.getMessage());
            } else if (e.getCode() == 4) {
                tk.logResult(MESSAGES.getString("msg3"));
            } else {
                tk.logResult(e.getMessage());
            }
        }

    }

    private int[] generateElements(int count) {
        int[] elements = new int[count];
        for (int i = 0; i < count; i++) {
            elements[i] = i;
        }
        Collections.shuffle(Arrays.asList(elements));
        Collections.shuffle(Arrays.asList(elements));
        return elements;
    }

    public void pradetiTyrima() {
        try {
            SisteminisTyrimas();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    public void pradetiIndividualuTyrima()
    {
        try {
            individualusMetodai();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }


    public void SisteminisTyrimas() throws InterruptedException {
        try {
            for (int k : TIRIAMI_KIEKIAI) {
                Autobusas[] autoMas = AutoGamyba.generuotiIrIsmaisyti(k, 1.0);
                aSeries.clear();
                aSeries2.clear();
                aSeries3.clear();
                tk.startAfterPause();
                tk.start();
                for (Autobusas a : autoMas) {
                    aSeries.add(a);
                }
                tk.finish(TYRIMU_VARDAI[0]);
                for (Autobusas a : autoMas) {
                    aSeries2.add(a);
                }
                tk.finish(TYRIMU_VARDAI[1]);
                for (Autobusas a : autoMas) {
                    aSeries3.add(a);
                }
                tk.finish(TYRIMU_VARDAI[2]);
                for (Autobusas a : autoMas) {
                    aSeries.remove(a);
                }
                tk.finish(TYRIMU_VARDAI[3]);
                tk.seriesFinish();
            }
            tk.logResult(FINISH_COMMAND);
        } catch (MyException e) {
            if (e.getCode() >= 0 && e.getCode() <= 3) {
                tk.logResult(errors[e.getCode()] + ": " + e.getMessage());
            } else if (e.getCode() == 4) {
                tk.logResult(MESSAGES.getString("msg3"));
            } else {
                tk.logResult(e.getMessage());
            }
        }
    }

    public BlockingQueue<String> getResultsLogger() {
        return resultsLogger;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }
}
